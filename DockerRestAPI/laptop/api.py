# Laptop Service

from flask import Flask
from flask_restful import Resource, Api
from flask_brevets import collection

# Instantiate the app
app = Flask(__name__)
api = Api(app)

opens = []
closes = []

for item in collection:
    opens.append(item['open'])
    closes.append(item['close'])

class Times(Resource):
    def get_open(self):
        return opens
    def get_close(self):
        return closes
    def get_both(self):
        return opens,closes

# Create routes
# Another way, without decorators
api.add_resource(Times, '/')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
