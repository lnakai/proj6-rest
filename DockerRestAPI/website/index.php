<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>Lists of Brevet Times</h1>
        <ul>
            <?php
            echo "Open Times";
            $json = file_get_contents('http://laptop-service/OTimes');
            $obj = json_decode($json);
	          $op = $obj->Opens;
            foreach ($op as $l) {
                echo "<li>$l</li>";
            }
            echo "Close Times";
            $json = file_get_contents('http://laptop-service/CTimes');
            $obj = json_decode($json);
	          $cl = $obj->Closes;
            foreach ($cl as $l) {
                echo "<li>$l</li>";
            }
            echo "Open and Close Times";
            $json = file_get_contents('http://laptop-service/COTimes');
            $obj = json_decode($json);
              $op = $obj->Opens;
              $cl = $obj->Closes;
            foreach ($op as $l=>$value) {
                echo "<li>$op[$l] <br> $cl[$l]</li>";
            }
            ?>
        </ul>
    </body>
</html>
